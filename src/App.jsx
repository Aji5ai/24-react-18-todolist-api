import { useEffect, useState } from "react";
import axios from "axios"; // faire dans le terminal : npm install axios
import "./App.css";
import TodoList from "./components/TodoList";

const API_URL = "https://jsonplaceholder.typicode.com/todos"; // route de l'API qu'on stocke dans une variable

function App() {
  const [todos, setTodos] = useState([]); // penser à importer useState et useEffect
  // on créé un state sur nos données todos, et une fonction qui permet d'actualiser ces données

  // TODO install and import axios, create an useEffect hook, and call the API
  useEffect(() => { //useEffect n'est lancé que quand le composant est monté ou qu'il est updaté
    axios.get(API_URL).then((response) => { //axios se charge de la requete fetch de façon plus simple
      setTodos(response.data); // ça retourne directement nos données en JSON, et elles sont stockées dans todos en changeant son state avec setTodos
    });
  }, []); // Si dependencies est vide (le tableau vide []), l'effet ne dépend d'aucune variable, et il s'exécutera une seule fois après le montage du composant. 
  // Si dependencies contient des variables, l'effet sera exécuté à chaque fois que l'une de ces variables change. Si dependencies est omis (à ne pas faire !) (ce qui est le cas avec useEffect(() => {...})), l'effet sera exécuté à chaque rendu.

  return (
    <div className="App">
      <TodoList todos={todos} />
    </div>
  );
}

export default App;
