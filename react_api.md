# React - Appel d'API

Utilise une API pour afficher une liste d'éléments

## Ressources

- [https://gitlab.com/bastienapp/react18-todolist-api](https://gitlab.com/bastienapp/react18-todolist-api)
- [Axios - Introduction](https://axios-http.com/fr/docs/intro)
- [Comment utiliser Axios avec React: Le Guide Complet](https://web.archive.org/web/20221123062007/https://www.freecodecamp.org/french/news/comment-utiliser-axios-avec-react-le-guide-complet-2021/)​
- [React - useEffect](https://react.dev/reference/react/useEffect)

## Contexte du projet

Une collègue a démarré une application, et n'a pas eu le temps de la finir. Il s'agirait d'appeler une **API** afin d'afficher une liste de choses à faire (*todo list*).

​Elle te donne le lien de son dépôt et te demande de le finir en son absence. Afin de t'aider, elle a commenté les endroits où tu devrais intervenir.

Commence par faire un fork du dépôt suivant : [https://gitlab.com/bastienapp/react18-todolist-api](https://gitlab.com/bastienapp/react18-todolist-api).

Ensuite clone ton fork en local.

​Voici ce qui a déjà été fait par ta collègue :

- `src/App.jsx` : à modifier, contient actuellement un state vide todos, qui devra contenir la liste des objets chargés de l'API
- `src/TodoList.jsx` : contient du code de l'affichage de la liste, il n'est pas à modifier
- `src/TodoItem.jsx` : contient le code nécessaire à l'affichage d'une tâche, il n'est pas à modifier​

Tu dois modifier le composant `App` afin de charger la liste des tâches à réaliser, à partir d'une **API** dont le lien est contenu dans la constante `API_URL`.
​
Pour charger l'API, tu devras utiliser la bibliothèque axios, que tu devras installer dans le projet à l'aide de la documentation officielle : [Axios - Introduction](https://axios-http.com/fr/docs/intro).

Pour charger une API, tu peux suivre l'explication de ce document (ou trouver tes propres ressources) : [Comment utiliser Axios avec React: Le Guide Complet](https://web.archive.org/web/20221123062007/https://www.freecodecamp.org/french/news/comment-utiliser-axios-avec-react-le-guide-complet-2021/)​​.

Ajoute des commentaires afin d'expliquer ton code.

Pour exécuter une action au montage d'un composant (la première fois qu'un composant est affiché), il faut utiliser le **hook** `useEffect` : [React - useEffect](https://react.dev/reference/react/useEffect).

Une fois l'API chargée, tu stockera ses données dans le state **todos**.​

## Modalités pédagogiques

- Un dépôt GitLab contient le code du projet
- Faire un fork du dépôt et cloner le fork en local
- Modifier le composant "App" afin de charger les éléments à partir d'une API
- Le hook "useEffect" est utilisé
- Le state "todos" est modifié

## Modalités d'évaluation

- La liste s'affiche bien à partir de l'objet todos
- Des commentaires expliquent le code
- Aucune erreur n'apparaît dans le terminal

## Livrables

- Un lien vers GitLab

## Critères de performance

- Le code source est documenté
- Utiliser les normes de codage du langage
- Utiliser un service distant Representational State Transfer Application Program Interface (API Rest)
- La documentation technique de l’environnement de travail est comprise
- Utiliser un outil de gestion de versions